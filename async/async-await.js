async function hello(name) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      console.log("Hello " + name);
      resolve(name);
    }, 1000);
  });
}

async function talk(name) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      console.log("Blah blah blah ....");
      resolve(name);
    }, 1000);
  });
}

async function bye(name) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      console.log("Bye " + name);
      resolve();
    }, 1000);
  });
}
console.log("Started process ....");
async function main() {
  let name = await hello("Oscar");
  await talk();
  await talk();
  await bye(name);
  console.log("Finished process of the Async await");
}
main();
console.log("Finished process");

function Asin(myCallback) {
  setTimeout(function () {
    console.log("Estoy siendo asíncrona");
    myCallback();
  }, 1000);
}
console.log("Iniciando proceso ....");
Asin(function () {
  console.log("Terminando el proceso ...");
});

function greetingHola(nombre, callback) {
  setTimeout(() => {
    console.log("Hola " + nombre);
    callback(nombre);
  }, 1000);
}

function greetingBye(nombre, callback) {
  setTimeout(() => {
    console.log("Adios " + nombre);
    callback();
  }, 1000);
}

console.log("Iniciando proceso ..");
greetingHola("Oscar", (nombre) => {
  greetingBye(nombre, () => {
    console.log("Terminando el proceso...");
  });
});

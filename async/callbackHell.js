function hello(name, callback) {
  setTimeout(() => {
    console.log("Hello " + name);
    callback(name);
  }, 1000);
}

function talk(callback) {
  setTimeout(() => {
    console.log("Blah blah blah ....");
    callback();
  }, 1000);
}

function bye(name, callback) {
  setTimeout(() => {
    console.log("Bye " + name);
    callback();
  }, 1000);
}

function conversation(name, times, callback) {
  if (times > 0) {
    talk(() => {
      conversation(name, --times, callback);
    });
  } else {
    bye(name, callback);
  }
}

console.log("Started process ....");

hello("Oscar", (name) => {
  conversation(name, 3, () => {
    console.log("Finished process ..");
  });
});

//! CallbackHell

hello("Franco", (name) => {
  talk(() => {
    talk(() => {
      talk(() => {
        bye(name, () => {
          console.log("Finished conversation...");
        });
      });
    });
  });
});

function hello(name) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      console.log("Hello " + name);
      resolve(name);
    }, 1000);
  });
}

function talk(name) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      console.log("Blah blah blah ....");
      resolve(name);
      //reject("An error ☹️");
    }, 1000);
  });
}

function bye(name) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      console.log("Bye " + name);
      resolve();
    }, 1000);
  });
}

console.log("Started process ....");

hello("Franco")
  .then(talk)
  .then(bye)
  /* .then((name) => {
    return bye(name);
  }) */
  .then(() => {
    console.log("Finished process");
  })
  .catch((error) => {
    console.error("An error has occurred");
    console.error(error);
  });

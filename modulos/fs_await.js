const fs = require("fs").promises;

async function read(path, code) {
  try {
    const data = await fs.readFile(path, code, (err, data) => {
      err
        ? console.error("Error in to arrow function readFile: " + err.message)
        : console.info("Data in to arrow function: " + data);
    });
    console.info("Data in to try: " + data);
  } catch (error) {
    console.error("Erro in to catch: " + error.message);
  }
}

async function write(path, content) {
  try {
    const data = await fs.writeFile(path, content, (err) => {
      err
        ? console.error(
            "Error of the write file in arrow function: " + err.message
          )
        : console.log("Successfully!");
    });
    console.info("Successfully in try!");
  } catch (error) {
    console.error("Erro in to catch: " + error.message);
  }
}

async function destroy(path, cb) {
  try {
    fs.unlink(path, cb);
    console.info("Successfully!");
  } catch (error) {
    console.error("An error occurred ☹️" + error);
  }
}

read(__dirname + "/archivo.txt", { encoding: "utf-8" });
write(__dirname + "/archivo2.txt", "Hi my friends i'm second file ...");
destroy(__dirname + "/archivo2.txt", console.log);

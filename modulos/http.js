const http = require("http");

http
  .createServer(function (req, response) {
    console.log("New request");
    console.log(req.url);
    response.writeHead(201, { "content-Type": "text/plain" });
    response.write("Hi from http ..");
    response.end();
  })
  .listen(3000);

console.log("Listen in port 3000");

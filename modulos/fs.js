const fs = require("fs");

function read(route) {
  fs.readFile(route, (err, data) => {
    err ? console.error(err.message) : console.info(data.toString());
  });
}

function write(route, content) {
  fs.writeFile(route, content, (err) => {
    err ? console.error(err.message) : console.log("Successfully!");
  });
}

function destroy(route, cb) {
  fs.unlink(route, cb);
}

read(__dirname + "/archivo.txt");
write(__dirname + "/archivo_1.txt", "Hi guys i'm new file ...");
destroy(__dirname + "/archivo_1.txt", console.log);

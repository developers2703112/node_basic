const { exec, spawn } = require("child_process");

exec("ls -la", (error, stdout, stderr) => {
  if (error) {
    console.error(error);
    return false;
  }
  console.log(stdout);
});

let process = spawn("ls", ["-la"]);

console.log(process.pid);
console.log(process.connected);

process.stdout.on("data", (d) => {
  console.log("¿Is Dead?");
  console.log(process.killed);
  console.log(d.toString());
});

process.on("exit", () => {
  console.log("The process finished");
  console.log(process.killed);
});

process.on("uncaughtException", (error, origen) => console.log(error, origen));
